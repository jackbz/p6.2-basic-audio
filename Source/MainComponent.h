/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                               AudioIODeviceCallback,
                               Slider::Listener
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    void resized() override;
    void audioDeviceIOCallback(const float **inputChannelData, int numInputChannels, float **outputChannelData, int numOutputChannels, int numSamples) override;
    void audioDeviceAboutToStart (AudioIODevice *device) override;
    void audioDeviceStopped() override;
    void sliderValueChanged (Slider* slider) override;

private:
    AudioDeviceManager audioDeviceManager;
    Slider amplitudeSlider;
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
