/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.addAudioCallback (this);
    
    addAndMakeVisible(amplitudeSlider);
    amplitudeSlider.setRange(0, 1);
    amplitudeSlider.setSliderStyle(Slider::LinearHorizontal);
    amplitudeSlider.addListener(this);
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeAudioCallback (this);
}

void MainComponent::resized()
{
    amplitudeSlider.setBounds(0, 0, getWidth(), 60);
}

void MainComponent::audioDeviceIOCallback(const float** inputChannelData,
                                          int numInputChannels,
                                          float** outputChannelData,
                                          int numOutputChannels,
                                          int numSamples)
{
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    while(numSamples--)
    {
        *outL = *inL *amplitudeSlider.getValue();
        *outR = *inR *amplitudeSlider.getValue();
        inL++;
        outL++;
        outR++;
    }
}

void MainComponent::audioDeviceAboutToStart (AudioIODevice *device)
{
    DBG ("Audio Device About to Start");
}
void MainComponent::audioDeviceStopped()
{
    DBG ("Audio Device Stopped");
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    DBG ("LOL");
}
